/*
* Simple Accelerometer Example
* */

package example.devesh.com.accelerometerexample;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener{

    SensorManager sensorManager;
    Sensor mySensor;
    TextView xValue, yValue, zValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize the sensorManager
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //Check if the accelerometer is not null
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)!= null)
        {
            /*Assign mySensor to accelerometer and register the sensorManager to the Listener */
            mySensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this,mySensor,SensorManager.SENSOR_DELAY_NORMAL);
        }

        xValue = (TextView) findViewById(R.id.textView1);
        yValue = (TextView) findViewById(R.id.textView2);
        zValue = (TextView) findViewById(R.id.textView3);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //Assign the value to the corresponding views
        xValue.setText(Float.toString(event.values[0]));
        yValue.setText(Float.toString(event.values[1]));
        zValue.setText(Float.toString(event.values[2]));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
