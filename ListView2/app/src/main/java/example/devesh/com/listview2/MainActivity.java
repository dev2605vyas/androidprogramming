package example.devesh.com.listview2;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView listView1;

    int[] songImages = {R.drawable.image1,R.drawable.image2,R.drawable.image3,
            R.drawable.image4,R.drawable.image5,R.drawable.image6
    };
    String[] songTitles;
    String[] songDescription;

    private songAdapter1 songAdapter1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        songTitles = getResources().getStringArray(R.array.titles);
        songDescription = getResources().getStringArray(R.array.description);

        listView1 = (ListView) findViewById(R.id.listView1);

        songAdapter1 songAdapter1 = new songAdapter1(getApplicationContext(),R.layout.single_row1);
        listView1.setAdapter(songAdapter1);

        int i=0;

        for(String titles: songTitles)
        {
            songDataProvider songDataProvider = new songDataProvider(songImages[i],titles,songDescription[i]);
            i++;
            songAdapter1.add(songDataProvider);
        }
        listView1.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = null;
        switch(position){
            case 0:
                intent = new Intent(MainActivity.this,secondActivity.class);
                break;
            case 1:
                intent = new Intent(MainActivity.this,secondActivity.class);
                break;
            case 2:
                intent = new Intent(MainActivity.this,secondActivity.class);
                break;
            case 3:
                intent = new Intent(MainActivity.this,secondActivity.class);
                break;
            case 4:
                intent = new Intent(MainActivity.this,secondActivity.class);
                break;
            case 5:
                intent = new Intent(MainActivity.this,secondActivity.class);
                break;
        }
        startActivity(intent);
    }
}
