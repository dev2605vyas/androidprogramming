package example.devesh.com.listview2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class songAdapter1 extends ArrayAdapter {

    List list1 = new ArrayList();

    public songAdapter1(Context context, int resource) {
        super(context, resource);
    }

    class DataHandler {
        ImageView poster;
        TextView title;
        TextView description;
    }

    @Override
    public void add(Object object)
    {
        super.add(object);
        list1.add(object);
    }

    @Override
    public int getCount() {
        return this.list1.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list1.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row;
        row = convertView;
        DataHandler dataHandler;

        if(row ==  null)
        {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.single_row1,parent,false);
            dataHandler = new DataHandler();

            dataHandler.poster = (ImageView)row.findViewById(R.id.imageView);
            dataHandler.title = (TextView)row.findViewById(R.id.textView1);
            dataHandler.description = (TextView)row.findViewById(R.id.textView2);

            row.setTag(dataHandler);
        }
        else
        {
            dataHandler = (DataHandler) row.getTag();
        }

        songDataProvider songDataProvider;
        songDataProvider = (songDataProvider)this.getItem(position);
        dataHandler.poster.setImageResource(songDataProvider.getSongPoster());
        dataHandler.title.setText(songDataProvider.getSongTitles());
        dataHandler.description.setText(songDataProvider.getSongDescription());

        return row;
    }

}
