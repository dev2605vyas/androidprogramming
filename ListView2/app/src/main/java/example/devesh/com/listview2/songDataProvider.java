package example.devesh.com.listview2;

/**
 * Created by deves on 10/24/2016.
 */
public class songDataProvider {

    private int songPoster;
    private String songTitles;
    private String songDescription;

    public songDataProvider(int songPoster, String songTitles, String songDescription) {
        this.setSongPoster(songPoster);
        this.setSongTitles(songTitles);
        this.setSongDescription(songDescription);
    }

    public int getSongPoster() {
        return songPoster;
    }

    public void setSongPoster(int songPoster) {
        this.songPoster = songPoster;
    }

    public String getSongTitles() {
        return songTitles;
    }

    public void setSongTitles(String songTitles) {
        this.songTitles = songTitles;
    }

    public String getSongDescription() {
        return songDescription;
    }

    public void setSongDescription(String songDescription) {
        this.songDescription = songDescription;
    }
}
