package example.kalpnik.com.listview1;

import android.graphics.drawable.Drawable;

public class SongDataProvider {
    private int songPoster;
    private String songTitles;

    public SongDataProvider(int songPoster, String songTitles) {
        this.setSongPoster(songPoster);
        this.setSongTitles(songTitles);
    }

    public int getSongPoster() {
        return songPoster;
    }

    public void setSongPoster(int songPoster) {
        this.songPoster = songPoster;
    }

    public String getSongTitles() {
        return songTitles;
    }

    public void setSongTitles(String songTitles) {
        this.songTitles = songTitles;
    }
}
