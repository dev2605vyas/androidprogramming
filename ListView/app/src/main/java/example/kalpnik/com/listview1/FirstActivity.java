package example.kalpnik.com.listview1;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class FirstActivity extends AppCompatActivity {

    //Initialize the ListView
    ListView list1;

    //Get the resources
    int[] songImages = {R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image4,
            R.drawable.image5,
            R.drawable.image6
    };
    String[] songTitles;

    //Initialize the SongAdapter
    SongAdapter songAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
        list1 = (ListView)findViewById(R.id.listView);
        songTitles = getResources().getStringArray(R.array.titles);
        int i=0;

        ////Initialize the songAdapter and set it to the list
        songAdapter = new SongAdapter(getApplicationContext(),R.layout.single_row_layout);
        list1.setAdapter(songAdapter);
        for(String title: songTitles)
        {
            SongDataProvider songDataProvider = new SongDataProvider(songImages[i],title);
            i++;
            songAdapter.add(songDataProvider); //Add the object to the songAdapter
        }
    }
}
