package example.kalpnik.com.listview1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SongAdapter extends ArrayAdapter{

    List list = new ArrayList(); //Get the list of the objects

    public SongAdapter(Context context, int resource)
    {
        super(context, resource);
    }

    static class DataHandler
    {
        ImageView poster;
        TextView title;
    }

    @Override
    public void add(Object object)
    {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row ;
        row= convertView;
        DataHandler dataHandler;

        if(convertView ==  null)
        {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.single_row_layout,parent,false);
            dataHandler = new DataHandler();

            dataHandler.poster = (ImageView)row.findViewById(R.id.imageView);
            dataHandler.title = (TextView)row.findViewById(R.id.textView);

            row.setTag(dataHandler);
        }
        else
        {
            dataHandler = (DataHandler) row.getTag();
        }

        SongDataProvider songDataProvider;
        songDataProvider = (SongDataProvider)this.getItem(position);
        dataHandler.poster.setImageResource(songDataProvider.getSongPoster());
        dataHandler.title.setText(songDataProvider.getSongTitles());

        return row;
    }
}
